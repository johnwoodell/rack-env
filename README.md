# rack-env
Evaluate rack environment in context of a docker image running behind an nginx-proxy.

### Setup host name

Add the hostname `rack-env.example.com` to your `/etc/hosts` file pointing to the Docker host to run these container.

### Launch containers

Run both containers to inspect the environment of the app running behind the nginx-proxy.

    docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy

    docker run -d -p 1234:9292 -e VIRTUAL_HOST=rack-env.example.com woodie/rack-env

### Inspect environment

Port 80 will use the nginx-proxy, port 1234 (in this example) connects directly to the rack-env container.

    http://rack-env.example.com:80

    http://rack-env.example.com:1234
