# config.ru
 
run Proc.new { |env|
  e = ["\n", '#### ENV ####', "\n"] + ENV.keys.sort.map {|k| "#{k}: #{ENV[k]}\n"} +
      ["\n", '#### env ####', "\n"] + env.keys.sort.map {|k| "#{k}: #{env[k]}\n"}
  ['200', {'Content-Type' => 'text/plain'}, e]
}
